import { BrowserRouter, Route, Routes } from "react-router-dom";
import Error from "./pages/Error";
import FilmList from "./pages/FilmList";
import FilmPage from "./pages/FilmPage/FilmPage";
import SharedLayout from "./pages/SharedLayout";
import './App.css'
function App() {

  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<SharedLayout/>}>
          <Route index  element={<FilmList/>}/>
          <Route path="/films/:kinopoiskId" element={<FilmPage/>}/>
          <Route path="*" element={<Error/>}/>
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;




  // const check =async () => {
  //   fetch(`${mainUrl}/v2.2/films/`, 
  //     {
  //       method: 'GET',
  //       headers: {
  //         'X-API-KEY': X_API_KEY,
  //         'Content-Type': 'application/json',
  //       },
  //     }
  //   )
  //   .then(res => res.json())
  //   .then(json => console.log(json))
  // }
  // check()

  