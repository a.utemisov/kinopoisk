import FilmListItem from "../components/FilmListItem"
import { useEffect, useState } from "react";
import axios from "axios";

import '../css/FilmList.css'

// const X_API_KEY = `${process.env.REACT_APP_X_API_KEY}`
const X_API_KEY2 = `30a19a7e-2a89-423f-8580-9b08fc1ca861`

const mainUrl = `https://kinopoiskapiunofficial.tech/api`


const FilmList = () => {
  
  const [filmlist, setFilms] = useState<any[]>([])

  const fetchAll = async () => {
    try {
      const response = await axios.get(
        `${mainUrl}/v2.2/films/`,
        {
          headers: {
            'X-API-KEY' : X_API_KEY2,
            Accept: 'application/json',
          },
        },
        );
      // console.log(response)
      setFilms(response.data.items)
    } catch (error) {
      // console.log(error)
    }
  }

  useEffect(()=>{
    fetchAll()
  },[])

  return (
    <div className="film_list">
      {filmlist.map((film) => {
        // if(film){
          return (
            <FilmListItem key={film.kinopoiskId} {...film}/>
          )
        // }
      })}
    </div>
  )
}

export default FilmList