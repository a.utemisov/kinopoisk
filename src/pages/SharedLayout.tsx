import { Outlet } from "react-router-dom"
import Navbar from "../components/Navbar"

const SharedLayout = () => {
  return(
    <div className="main_container">
      <Navbar/>
      <Outlet/>
    </div>
  )
}

export default SharedLayout