import { useParams } from "react-router-dom"
import React, { useEffect, useState } from "react";
import axios from "axios";
import '../../css/FilmPage.css'
import { Menu } from "antd";
// import { MailOutlined, AppstoreOutlined, SettingOutlined } from '@ant-design/icons';
import FilmPageInfo from "./FilmPageInfo";
import  'antd/dist/antd.css';

// import { url } from "inspector";

const X_API_KEY = `${process.env.REACT_APP_X_API_KEY}`
const mainUrl = `https://kinopoiskapiunofficial.tech/api`
// const X_API_KEY = `30a19a7e-2a89-423f-8580-9b08fc1ca861`

// const fetchFilmStuff = async()=>{
//   try {
//     const response = await axios(
//       `${mainUrl}/v1/staff?filmId=${kinopoiskId}`,
//       {
//         headers: {
//           'X-API-KEY' : X_API_KEY,
//           Accept: 'application/json',
//         },
//       },
//     )

// response.data.map((person:any)=>{
//   switch (person.professionKey) {
//     // case "ACTOR":
//     //   _actors.push(person)
//     //   break;
//     case "DIRECTOR":
//       console.log(person.professionKey)
//       setDirectors((old:any) => [...old, person])
//       break;
//     // case "PRODUCER":
//     //   _producers.push(person)
//     //   break;
//     default:
//       break; 
//   }

//       return null
//     })
//   } catch (error) {
  
  //   }
  // }
  
  
  const fetchAll =async (kinopoiskId:any, func:any) => {
    try {
    const info = await axios.get(
      `${mainUrl}/v2.2/films/${kinopoiskId}`,
      {
        headers: {
          'X-API-KEY' : X_API_KEY,
          Accept: 'application/json',
        },
      },
    )
    const stuff = await axios(
      `${mainUrl}/v1/staff?filmId=${kinopoiskId}`,
      {
        headers: {
          'X-API-KEY' : X_API_KEY,
          Accept: 'application/json',
        },
      },
    )
    const images = await axios.get(
      `${mainUrl}/v2.2/films/${kinopoiskId}/images`,
      {
        headers: {
          'X-API-KEY' : X_API_KEY,
          Accept: 'application/json',
        },
      },
    )
    return ({info, images, stuff})
  } catch (error) {
  }
}

//MAIN FUNCTION
const FilmPage = () => {
  const {kinopoiskId} = useParams();  
  const [film, setFilm] = useState<any>(0)
  const [filmImg, setFilmImg] = useState<any>()
  const [menuPage, setMenuPage] = useState<string>(`Обзор`)
  const [actorsPage, setActorsPage] = useState<number>(-1)

  const [_directors, setDirectors] = useState<any[]>([null])
  const [_directorsOnPage, setDirectorsOnPage] = useState<any>([])

  const [_actors, setActors] = useState<any[]>([null])
  const [_actorsOnPage, setActorsOnPage] = useState<any>([])

  const setFilms = () => {
    fetchAll(kinopoiskId, setActorsOnPage)
    .then(
      resp =>{
        setFilm(resp?.info)
        setFilmImg(resp?.images)
        setDirectors(resp?.stuff.data.filter((person:any)=>
          person.professionKey === 'DIRECTOR'
        ))
        setActors(resp?.stuff.data.filter((person:any)=>
          person.professionKey === 'ACTOR'
        ))

      }
    )
  }

  useEffect(()=>{
    setFilms()
  },[]) 

  useEffect(()=>{
    if(actorsPage>=0)
      for(let i = 0 + 10 * actorsPage; i<10+10*actorsPage; i++){
        console.log(i)
        if(!_actorsOnPage)  
          setActorsOnPage(_actors[i])
        else if(_actors[i])
          setActorsOnPage((old:any)=>[...old, _actors[i]])
      }

    for(let i=0; i<2; i++){
      setDirectorsOnPage((old:any)=>[...old, _directors[i]])
    }
  },[actorsPage])

  const pageMenuHandler = (e:any) => {
    setMenuPage(e.target.innerHTML)
  }

  const renderMenu = ()=> {
    switch (menuPage) {
      case "Обзор":
        return (
          <div>
            {film.data.shortDescription}
          </div>
        )
      case "Изображения":
        return (
          <div className="film_image_container">
            {filmImg.data.items.map((item:{previewUrl:string}) => {
              return(
                <img key={Math.random()} className="image_item" src={`${item.previewUrl}`} alt=''/>
              )
            })}
          </div>
        )
      case "В Ролях":{
        return (
          <div className="film_actor_container">
            {_actorsOnPage.map((actor:any) => {
              return(
                <div className="actor_list" key={Math.random()}>
                  <img  className="actor_img" src={actor.posterUrl} alt=''/>
                  <span className="actor_name">{actor.nameRu}</span>
                  <span className="actor_name">{actor.description}</span>
                </div>
              )
            })}
            <button onClick={()=>setActorsPage(actorsPage+1)}>More</button>
          </div>
        )}
      default:
        return <div>Error</div>
    }
  }
  
  if(film && filmImg)
  return (
    <section className='film_page'>
      <FilmPageInfo film={film} filmImg={filmImg} directors={_directorsOnPage}/>

      <Menu mode="horizontal" defaultSelectedKeys={["Обзор"]}>
        <Menu.Item key="Обзор" >
          <span onClick={pageMenuHandler}>Обзор</span>
        </Menu.Item>
        <Menu.Item key="Изображения" >
          <span onClick={pageMenuHandler}>Изображения</span>
        </Menu.Item>
        <Menu.Item key="В Ролях">
          <span onClick={(e) => {pageMenuHandler(e);}}>В Ролях</span>
        </Menu.Item>
      </Menu>


      <div className='menu'>
        {renderMenu()}
      </div>

    </section>
  )
  return(
    <div>Loading...</div>
  )
}
export default FilmPage