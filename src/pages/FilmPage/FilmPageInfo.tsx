const FilmPageInfo = (params: any) => {
  return( 
    <div className='film_info_container'>
      <img src={params.film.data.posterUrl} alt='' className='film_poster_url'/>
      <div className='film_info'>
        <h2>{params.film.data.nameRu || params.film.data.nameOriginal}</h2>

      {/* Страна */}
        <div className="film_info_span">
          <span>Страна : </span>
          <span>
            {params.film.data.countries.map((count:{country:string}) => <span className='film_info_span_item' key={Math.random()}>{count.country}</span>)}
          </span>
        </div>

        {/* Жанры */}
        <div className="film_info_span">
          <span>Жанры : </span>
          <div className="film_info_list">
            {params.film.data.genres.map((genre:{genre:string}) => {
              return(
                <span className='film_info_span_item' key={Math.random()}>{genre.genre}</span>
              )
            })}
          </div>
        </div>

        {/* Год производства */}
        <div className="film_info_span">
          <span>Год производства : </span>
          <span>{params.film.data.startYear}</span>
        </div>

        {/* Слоган */}
        <div className="film_info_span">
          <span>Слоган :</span>
          <span>{params.film.data.slogan}</span>
        </div>

        {/* Режиссер */}
        <div className="film_info_span">
          <span>Режиссер</span>
          {params.directors.map((director:any) => {
            if(director)
            return(
              <span className="film_info_list" key={Math.random()}>{director.nameRu}</span>
            )
          })}
        </div>

        <div className="film_info_span">
          <span></span>
        </div>
        <div className="film_info_span">
          <span></span>
        </div>
        <div className="film_info_span">
          <span></span>
        </div>
      </div>
    </div>
  )
}

export default FilmPageInfo