import React from "react"
import { Link } from "react-router-dom"
import '../css/FilmListItem.css'
// interface FilmListItemProps {
//   film: {kinopoiskId: number, title: string},
// }

const FilmListItem= (film: {
  kinopoiskId:number, 
  nameRu: string,
  nameOriginal: string,
  posterUrl: string,
  genres: [{genre: string}]
}) => {
  const genres = film.genres
  return (
    <section className='film_list_item'>
      <div className='film_list_container'>
        <img src={film.posterUrl} alt='' className='poster_url'/>
          <div>
            <Link to={`/films/${film.kinopoiskId}`}>{film.nameRu || film.nameOriginal}</Link>
            {/* <a href={`/films/${film.kinopoiskId}`}>{film.nameRu}</a> */}
            <div>Lorem ipsum dolor sit amet consectetur adipisicing elit. Eius alias voluptates, inventore porro vero earum sapiente voluptas rerum quos vel culpa quidem ipsam, voluptatem illum architecto ad deserunt, ipsum quas.</div>
          </div>
      </div>
      <div className='film_genres'>
        {genres.map(genre => {
          return(
            <span className='film_genre' key={Math.random()}>{genre.genre}</span>
          )
        })}
      </div>
    </section>
  )
}

export default FilmListItem