import { Link } from "react-router-dom"
import '../css/Navbar.css'
const Navbar = () => {
  return(
    <section className="navbar_container">
      <div className="navbar_list">
        <div className="navbar_list_container">
          
          <Link to={'/'}>Главная</Link>
          <Link to={'#'}>Че-то</Link>
          <Link to={'#'}>Еще Че-то</Link>
          <input placeholder="search"/>
        </div>
      </div>
    </section>
  )
}

export default Navbar